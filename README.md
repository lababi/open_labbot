#Open_LabBot

This repository contains C++ code for controlling a G-code based sampling robot, a laser and the Thermo LCQ Fleet data acquisition software.

![][fig:ScreenShot]
[fig:ScreenShot]: ./ScreenShot.png
Figure 1. *LABI-Imaging User interface*

LABI-Imaging is a C++ program for mass spectrometry imaging (MSI) data acquisition (synchronization of robot movements and MS data acquisition, control via the USB port). LABI-Imaging works in 32/64 bit windows operative systems and it is entirely based on windows API, e.g., it does not use MFC, dot NET or any other intermediary class support; however, the latest window's Software Development Kit (SDK) must be installed and available while compiling.


LABI-Imaging is able to perform several control tasks relevant for MSI acquisition: 

    1. Sending requests to the laser controller for establishing the power output level.
    2. Moving LabBot platform using G-Code commands trough an USB interface.
    3. Setting the spectrum file name.
    4. Request a scanning start/end to the Thermo LCQ Fleet MS software.
    5. Monitoring the busy/end status for an acquisition process.

By combining these features, two MSI acquisition algorithms were developed, see figure [2][fig:FlowDiagram]. Although LABI-Imaging was specifically designed for working in conjunction with Thermo LCQ Fleet software, it should be possible to modify the program in order to work with another vendor's instruments.

![][fig:FlowDiagram]
[fig:FlowDiagram]: ./fig-MSI-flow.png

Figure 2. *LABI-Imaging MSI acquisition methods: a) The noncontinuous method stores a spectrum for each pixel, where the spectrum file name is related to spatial information, b) Spectral information is stored into a single file; a separated text file is created for storing the time stamp of every robot movement.*

The first MSI acquisition algorithm request a new spectrum file for each image's spatial position, where the spectrum file name contains the corresponding s-shape scanning index. Under this method MSI reconstruction is straightforward, however has a main draw back: it is a slow procedure. In our tests the best spectral acquisition rate is about 20 spectra every minute; under this conditions, scanning an image of only 30 X 60 pixels will take a full hour. With minimal modifications, this algorithm can be adapted to optimize laser operation conditions such as focusing distance and output power.

The second algorithm stores several micro scans into a single file, which generates a better scan rate throughput. A delay is introduced between each platform movement in order to approximate as close as possible the time needed to measure one or more scans per pixel. Under this method, image reconstruction is more elaborated due to two factors. On one hand, spatial and spectral information can only be related using a time stamp recorded when the platform is about to move; on the other hand, mechanical delay does not always match with scanning acquisition times, thus an interpolation procedure must be done for scans that were measured when the platform was traveling from one to another pixel.

The non-continuous acquisition is a robust and straightforward method that can be used with low resolution images or when sample integrity is not compromised with long sampling periods. In this work, automatic essential oil spectral acquisition employs this method. When sample degradation occurs at faster time rates than the total measuring time, continuous acquisition method should be used instead.


##Source code organization

LABI-Imaging was originally compiled with Visual C++ 2008 Express Edition. Please keep in mind that the application's message bomb uses the ``__asm`` directive which is not supported on latest releases of visual C++; however, changing the program's message bomb for handling notifications by means of a ``switch`` structure is an easy task. Alternatively, the assembler message bomb routine could be compiled with Microsoft Macro Assembler (MASM) with little modifications. Following we briefly describe the source code included on each folder.


###(Release)
Compiled versions of libraries, LABI-Imaging.exe and Laser.exe can be found in this folder.

###_library

Source code on this directory has general use routines for supporting Laser.exe ans MSI-Imaging.exe programs

*CCoord.cpp*  Class for converting coordinates between decimal and text. CCoord class uses fixed point arithmetic, i.e., an integer 123 represents 1.23

*CDrawTable.cpp*  Class for displaying headers and coordinates inside a window.

*CSerial.cpp*
Serial port control routines

*CSnoop.cpp*
Class for obtaining external application window's handle.

*Window.cpp*
Routines for registering and handling window messages. Also contains some cosmetic routines for changing window icon, fonts, etc.


###_include

This directory contains the header files for using library classes in _library directory.


###Laser

This project creates laser.exe application. In our equipment, the laser intensity is controlled trough a voltage level input. The Laser.exe program just sends an integer value via USB port to an Arduino UNO, in order to configure an external digital to analog converter.



###Imaging

This project creates LABI-Imaging application. Its main goal is to simplify the acquisition of MS images, by controlling the LabBot movements and supervising the acquisition status of Thermo LCQ Fleet acquisition software. Laser control can also be done from this program.

LABI-Imaging overrides the applicattion's system menu, in order to give acces to diferrent program options, such as laser parameters optimization routines, aquisition modes, etc.

*CWControl*   LABI-Imaging implements an owner-draw control for controlling LabBot's movements

*CDlgAbout*  Manages the about dialog box

*CDlgHelp*  Provides a dialog box with a brief description of LABI-Imaging capabilities


