#include <windows.h>
#include <tchar.h>

#include "..\\_include\\TWindow.h"
#include "CDlgAbout.h"
#include "resource.h"

#define  DLG_MARGIN  5
#define  TXT_XSTART  48 + 3 * DLG_MARGIN

// **************************************************************************************
// AREA DE DATOS
// **************************************************************************************

CDlgAbout::EVENT  CDlgAbout::SystemEvents[] = {
	WM_DRAWITEM,   reinterpret_cast<CMsg::HANDLER>( &CDlgAbout::OwnerDraw ),
	WM_ERASEBKGND, reinterpret_cast<CMsg::HANDLER>( &CDlgAbout::EraseBkgnd ),
	WM_INITDIALOG, reinterpret_cast<CMsg::HANDLER>( &CDlgAbout::InitDialog ),
	WM_NCDESTROY,  reinterpret_cast<CMsg::HANDLER>( &TWindow<CDlgAbout>::NCDestroy )
};

CDlgAbout::EVENT  CDlgAbout::CommandEvents[] = {
	(BN_CLICKED << 16) | IDOK,     reinterpret_cast<CMsg::HANDLER>( &CDlgAbout::OkCancel ),
	(BN_CLICKED << 16) | IDCANCEL, reinterpret_cast<CMsg::HANDLER>( &CDlgAbout::OkCancel )
};


CDlgAbout::EVENT_INFO  CDlgAbout::EventInfo[] = {
	SystemEvents,  sizeof(SystemEvents)/sizeof(EVENT),
	CommandEvents, sizeof(CommandEvents)/sizeof(EVENT)
};



// **************************************************************************************
// PROCESO DE EVENTOS DEL SISTEMA
// **************************************************************************************
LRESULT CDlgAbout::InitDialog( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	CenterWindow(hwnd);

	hInstance = (HINSTANCE) GetWindowLong( hwnd, GWL_HINSTANCE );
	SetIconWindow( hInstance, hwnd, IDI_ICON1 );

	// Obtiene el nombre de la fuente
	union { 
		LRESULT  msgReturn;
		HFONT    hFont;
	};

	msgReturn = SendMessage( hwnd, WM_GETFONT, 0, 0 );

	// Calculo en pixeles de la longitud de la linea mas larga
	SIZE    size;
	TCHAR   string[128];
	UINT    strlen = LoadString( hInstance, IDS_LABI1, string, 128 );
	HDC     hdc    = GetDC( hwnd );
	HGDIOBJ oldFnt = SelectObject( hdc, hFont );

	GetTextExtentPoint32( hdc, string, strlen, &size );
	LineHeight = size.cy * 11 / 8;

	// Libera hdc
	SelectObject( hdc, oldFnt );
	ReleaseDC( hwnd, hdc );

	// Modifica el tama�o del control
	HWND hwndChild  = GetDlgItem( hwnd, IDC_ABOUT );
	UINT ctrlWidth  = size.cx + DLG_MARGIN * 4 + 48;
	UINT ctrlHeight = LineHeight * 15;
	SetWindowRect( hwndChild, ctrlWidth, ctrlHeight );

	// Modifica el tama�o de la ventana
	RECT rc;
	GetControlRect( hwndChild, &rc );
	SetWindowRect( hwnd, ctrlWidth + rc.left * 2, ctrlHeight + rc.top * 2 );

	return TRUE;
}


LRESULT CDlgAbout::OkCancel( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	EndDialog( hwnd, TRUE );
	return TRUE;
}


LRESULT CDlgAbout::EraseBkgnd( HWND hwnd, UINT msg, HDC hdc, LPARAM lparam ) {

	// Obtiene el tama�o de la ventana
	RECT rc;
	GetClientRect( hwnd, &rc );

	// Limpia el fondo
	HGDIOBJ oldBrush = SelectObject( hdc, GetStockObject( WHITE_BRUSH ) );
	PatBlt( hdc, 0, 0, rc.right, rc.top, PATCOPY );
	SelectObject( hdc, oldBrush );
	return TRUE;
}


LRESULT CDlgAbout::OwnerDraw( HWND hwnd, UINT msg, WPARAM wparam, DRAWITEMSTRUCT *draw ) {

	// Crea un dispositivo de contexto en memoria
	HDC sourceDC = CreateCompatibleDC( draw->hDC );

	// Obtiene la instancia del programa y carga el logo
	HBITMAP   cinvestav = LoadBitmap( hInstance, MAKEINTRESOURCE( IDB_BITMAP5 ) );

	// Dibuja el logo y libera el recurso
	SelectObject( sourceDC, cinvestav );
	BitBlt( draw->hDC, DLG_MARGIN, DLG_MARGIN, 48, 47, sourceDC, 0, 0, SRCCOPY );
	DeleteObject( cinvestav );

	// Carga el logo de LABI
	UINT secondBlock = 4 * LineHeight + DLG_MARGIN * 2;
	HBITMAP labi = LoadBitmap( hInstance, MAKEINTRESOURCE( IDB_BITMAP4 ) );
	SelectObject( sourceDC, labi );
	BitBlt( draw->hDC, DLG_MARGIN, DLG_MARGIN + secondBlock, 48, 33, 
		    sourceDC, 0, 0, SRCCOPY );
	DeleteObject( labi );

	// Despliega el texto
	TCHAR string[128];
	COLORREF oldColor = SetBkColor( draw->hDC, 0xFFFFFF );

	UINT  accum  = DLG_MARGIN;
	UINT  strlen = LoadString( hInstance, IDS_CINVESTAV1, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight;
	strlen = LoadString( hInstance, IDS_CINVESTAV2, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight;
	strlen = LoadString( hInstance, IDS_CINVESTAV3, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum  = secondBlock;
	strlen = LoadString( hInstance, IDS_LABI1, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight;
	strlen = LoadString( hInstance, IDS_LABI2, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight;
	strlen = LoadString( hInstance, IDS_LABI3, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight * 2 + DLG_MARGIN;
	strlen = LoadString( hInstance, IDS_ABOUT1, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight;
	strlen = LoadString( hInstance, IDS_ABOUT2, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight * 2;
	strlen = LoadString( hInstance, IDS_ABOUT3, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	accum += LineHeight;
	strlen = LoadString( hInstance, IDS_ABOUT4, string, 128 );
	TextOut( draw->hDC, TXT_XSTART, accum, string, strlen );

	// Restaura el estado del hdc
	DeleteDC( sourceDC );
	SetBkColor( draw->hDC, oldColor );
	return TRUE;
}