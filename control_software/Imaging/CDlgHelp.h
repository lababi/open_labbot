// **************************************************************************************
// DEFINICION DE LA CLASE CDlgHelp
// **************************************************************************************

class CDlgHelp : protected CMsg {

protected:

	 CDlgHelp() {}
	~CDlgHelp() {}

private:

	LRESULT InitDialog( HWND, UINT, WPARAM, LPARAM );
	LRESULT Show( HWND, UINT, WPARAM, LPARAM );
	LRESULT BkgndColor( HWND, UINT, HDC, HWND );
	LRESULT OkCancel( HWND, UINT, WPARAM, LPARAM );

protected:

	static EVENT       SystemEvents[];
	static EVENT       CommandEvents[];
	static EVENT_INFO  EventInfo[];

	HGDIOBJ hBrush;

	CWndProcThunk Thunk;
};
