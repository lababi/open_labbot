// **************************************************************************************
// DEFINICION DE LA CLASE CWControl
// **************************************************************************************

class CWControl : protected CMsg {

	union CTime;

protected:

	 CWControl() {}
	~CWControl() {}

private:
	
	LRESULT Create( HWND, UINT, WPARAM, LPCREATESTRUCT );
	LRESULT NCDestroy( HWND, UINT, WPARAM, LPARAM );
	LRESULT Paint( HWND, UINT, WPARAM, LPARAM );
	LRESULT LButtonUp( HWND, UINT, WPARAM, LPARAM );
	LRESULT LButtonDown( HWND, UINT, WPARAM, LPARAM );
	LRESULT KeyUp( HWND, UINT, WPARAM, LPARAM );
	LRESULT KeyDown( HWND, UINT, WPARAM, LPARAM );
	LRESULT Focus( HWND, UINT, WPARAM, LPARAM );
	LRESULT GetDlgCode( HWND, UINT, WPARAM, LPARAM );
	LRESULT SetText( HWND, UINT, WPARAM, TCHAR* );
	LRESULT Enable( HWND, UINT, WPARAM, LPARAM );
	LRESULT InitMakerMex( HWND, UINT, WPARAM, LPARAM );
	LRESULT TestRegion( HWND, UINT, WPARAM, LPARAM );
	LRESULT SetupScans( HWND, UINT, WPARAM, LPARAM );
	LRESULT NextScan( HWND, UINT, WPARAM, LPARAM );
	LRESULT SetupSpectrum( HWND, UINT, WPARAM, LPARAM );
	LRESULT NextSpectrum( HWND, UINT, WPARAM, LPARAM );
	LRESULT Alert( HWND, UINT, WPARAM, LPARAM );
	LRESULT SetAmplitude( HWND, UINT, WPARAM, TCHAR * );

	void    InitTables();
	void    MeasureColumns ( HDC, HFONT );
	void    ChangeBackground( HINSTANCE, HDC, HDC );
	void    LoadArrow( HINSTANCE );
	void    SetKeyIncrement( UINT, UINT, UINT, UINT );
	void    DrawState( HDC, UINT, UINT );
	void    UpdateRegion( HDC, UINT );
	void    UpdateStats( HDC );
	void    ScanSetup();
	void    PixelIndex();
	void    ActiveThermo();
	void    CloseExperiment();

	UINT    IsThermoBusy();
	UINT    InitializeExperiment();
	UINT    CreateSynchroFile();
	UINT    UpdateSpectrum();
	UINT    UpdateElevation();
	UINT    UpdateLaser();
	UINT    SendCommand( char*, UINT );
	UINT    Num2String( UINT field, UINT option );

protected:

	static EVENT       SystemEvents[];
	static EVENT_INFO  EventInfo[];

	CWndProcThunk Thunk;

private:

	static BYTE  Mask[];
	static UINT  Column[];  
	static UINT  KeyToIndex[];
	static CDrawTable::TString  Label[];
	
	CDrawTable::TString  Values[28];
	TCHAR                Render[20][8];
	UINT                 Store[20];

	CSnoop      Thermo;
	CSerial     Robot;
	CSerial     Laser;
	CDrawTable  ShowCoord;
	CDrawTable  ShowRegion;
	CDrawTable  ShowStats;
	CCoord     *Position;
	__int64     StartTime;

	UINT        IsFocusOwner;
	UINT        IsWorking;
	UINT        IsWaiting;
	UINT        KeyCode;
	UINT        Step[2];
	UINT        Coord[3];
	UINT        Col;
	UINT        Row;
	UINT        DeltaZ;
	UINT        LaserPower;
	UINT        FileID;

	HANDLE      hFile;
	HWND        HwndParent;
	HWND        hWnd;
	HBITMAP     Arrow;
	HBITMAP     ArrowGray;
	HBITMAP     Display;

	UINT (CWControl::*Update)();
};


union CWControl::CTime {
	FILETIME  Time;
	__int64   Utc;
};