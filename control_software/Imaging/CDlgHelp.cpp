#include <windows.h>
#include <tchar.h>

#include "..\\_include\\TWindow.h"
#include "CDlgHelp.h"
#include "resource.h"


// **************************************************************************************
// AREA DE DATOS
// **************************************************************************************

CDlgHelp::EVENT  CDlgHelp::SystemEvents[] = {
	WM_INITDIALOG, reinterpret_cast<CMsg::HANDLER>( &InitDialog ),
	WM_SHOWWINDOW, reinterpret_cast<CMsg::HANDLER>( &Show ),
	WM_CTLCOLORSTATIC, reinterpret_cast<CMsg::HANDLER>( &BkgndColor ),
	WM_NCDESTROY,  reinterpret_cast<CMsg::HANDLER>( &TWindow<CDlgHelp>::NCDestroy )
};

CDlgHelp::EVENT  CDlgHelp::CommandEvents[] = {
	(BN_CLICKED << 16) | IDCANCEL, reinterpret_cast<CMsg::HANDLER>( &OkCancel )
};

CDlgHelp::EVENT_INFO  CDlgHelp::EventInfo[] = {
	SystemEvents,  sizeof(SystemEvents) / sizeof(EVENT),
	CommandEvents, sizeof(CommandEvents) / sizeof(EVENT)
};


// **************************************************************************************
// PROCESO DE EVENTOS DEL SISTEMA
// **************************************************************************************

LRESULT CDlgHelp::InitDialog( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Determina las dimensiones de la caja de dialogo
	HWND hwndChild  = GetDlgItem( hwnd, IDC_EDIT1 );
	UINT ctrlWidth  = GetSystemMetrics( SM_CXSCREEN ) / 3;
	UINT ctrlHeight = GetSystemMetrics( SM_CYSCREEN ) * 2 / 3;

	// Modifica las dimensiones de las ventanas
	RECT rc;
	GetControlRect( hwndChild, &rc );

	SetControlSize( hwndChild, ctrlWidth, ctrlHeight );
	SetWindowRect( hwnd, ctrlWidth + rc.left * 2, ctrlHeight + rc.top * 2 );
	CenterWindow( hwnd );

	// Carga el contenido del archivo
	HINSTANCE instance = (HINSTANCE) GetWindowLong( hwnd, GWL_HINSTANCE );
	HGLOBAL  hResource = 
		LoadResource( 
			instance,
			FindResource( instance, MAKEINTRESOURCE( IDT_HELP ), _T("TEXT") ) );

    char *pText = reinterpret_cast<char *>( LockResource( hResource ) );

	// Actualiza el texto
	SendMessageA( hwndChild, WM_SETTEXT, 0, reinterpret_cast<LPARAM>( pText ) );
	FreeResource( hResource );

	// Modifica el icono
	SendMessage( hwnd, WM_SETICON, ICON_SMALL, 
		reinterpret_cast<LPARAM>( LoadIcon( 0, MAKEINTRESOURCE( 32514 ) ) ) );

	// Crea una brocha para el fondo
	hBrush = GetStockObject( WHITE_BRUSH );

	return TRUE;
}


LRESULT CDlgHelp::Show( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Valida la comunicacion del puerto serial dentro del control de movimiento
	HWND hChild = GetDlgItem( hwnd, IDC_EDIT1 );
	PostMessage( hChild, EM_SETSEL, 0, 0 );
	return TRUE;
}


LRESULT CDlgHelp::BkgndColor( HWND hwnd, UINT msg, HDC hdc, HWND hwndChild ) {
	
	return reinterpret_cast<LRESULT>( hBrush );
}


LRESULT CDlgHelp::OkCancel( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	EndDialog( hwnd, TRUE );
	return TRUE;
}
