// **************************************************************************************
// DEFINICION DE LA CLASE CMainDlg
// **************************************************************************************

class CDlgMain : protected CMsg {

protected:

	 CDlgMain() {}
	~CDlgMain() {}

private:

	LRESULT InitDialog( HWND, UINT, WPARAM, LPARAM );
	LRESULT Cancel( HWND, UINT, WPARAM, LPARAM );
	LRESULT Show( HWND, UINT, WPARAM, LPARAM );
	LRESULT StartUp( HWND, UINT, WPARAM, LPARAM );
	LRESULT Send( HWND, UINT, WPARAM, LPARAM );
	LRESULT Activate( HWND, UINT, WPARAM, LPARAM );
	LRESULT Validate( HWND, UINT, WPARAM, LPARAM );

private:

	UINT     Intensity;
	CSerial  Serial;

protected:

	static EVENT       SystemEvents[];
	static EVENT       CommandEvents[];
	static EVENT_INFO  EventInfo[];

	CWndProcThunk Thunk;
};
