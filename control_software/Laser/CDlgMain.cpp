#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "..\\_include\\CSerial.h"
#include "..\\_include\\TWindow.h"
#include "CDlgMain.h"
#include "resource.h"

#define IDC_STARTUP 1000


// **************************************************************************************
// AREA DE DATOS
// **************************************************************************************

CDlgMain::EVENT  CDlgMain::SystemEvents[] = {
	WM_INITDIALOG,  reinterpret_cast<CMsg::HANDLER>( &CDlgMain::InitDialog ),
	WM_NCDESTROY,   reinterpret_cast<CMsg::HANDLER>( &TWindow<CDlgMain>::NCDestroy ),
	WM_SHOWWINDOW,  reinterpret_cast<CMsg::HANDLER>( &TWindow<CDlgMain>::Show )
};

CDlgMain::EVENT  CDlgMain::CommandEvents[] = {
	(BN_CLICKED << 16) | IDC_SEND,  reinterpret_cast<CMsg::HANDLER>( &CDlgMain::Send ),
	(BN_CLICKED << 16) | IDC_ONOFF, reinterpret_cast<CMsg::HANDLER>( &CDlgMain::Activate ),
	(BN_CLICKED << 16) | IDCANCEL, reinterpret_cast<CMsg::HANDLER>( &CDlgMain::Cancel ),
	(BN_CLICKED << 16) | IDC_STARTUP, reinterpret_cast<CMsg::HANDLER>( &CDlgMain::StartUp ),
	(EN_KILLFOCUS << 16) | IDC_EDIT1, reinterpret_cast<CMsg::HANDLER>( &CDlgMain::Validate ),
};

CDlgMain::EVENT_INFO  CDlgMain::EventInfo[] = {
	SystemEvents,  sizeof( SystemEvents ) / sizeof( EVENT ),
	CommandEvents, sizeof( CommandEvents ) / sizeof( EVENT )
};



// **************************************************************************************
// PUNTO DE ENTRADA AL PROGRAMA
// **************************************************************************************

extern "C" int WINAPI WinMain
( HINSTANCE instance, HINSTANCE hPrevInst, PSTR szCmdLine, int iCmdShow ) {

	TWindow<CDlgMain>::DlgBox( instance, IDD_DIALOG1, GetDesktopWindow(), SDlgProc );
	return 0;
}



// **************************************************************************************
// PROCESO DE EVENTOS DEL SISTEMA
// **************************************************************************************

LRESULT CDlgMain::InitDialog( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Cambia el icono y centra la ventana
	HINSTANCE instance = (HINSTANCE) GetWindowLong( hwnd, GWL_HINSTANCE );
	SetIconWindow( instance, hwnd, IDI_ICON1 );
	CenterWindow( hwnd );

	// Intensidad = 0 (se establece por defecto en el programa de Arduino)
	Intensity = 0;

	// Limita el texto a dos caracteres y coloca el caracter inicial en cero
	SendDlgItemMessage( hwnd, IDC_EDIT1, EM_SETLIMITTEXT, 3, 0 );
	SetDlgItemInt( hwnd, IDC_EDIT1, 0, 0 );

	// Conexion con el puerto serial
	Serial.EnumArduinoPorts( ARDUINO_UNO );
	UINT isConected = Serial.IsConnected() == 0;

	if( isConected )
		ShowAlert( hwnd, IDS_SERIAL, IDS_SERIALOPEN );

	return isConected;
}


LRESULT CDlgMain::Show( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	PostMessage( hwnd, WM_COMMAND, (BN_CLICKED << 16) | IDC_STARTUP, 0 );
	return DefWindowProc( hwnd, msg, wparam, lparam );
}


LRESULT CDlgMain::StartUp( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	LONG cursor = reinterpret_cast<LONG>
		          ( LoadCursor( 0, MAKEINTRESOURCE( IDC_ARROW ) ) );

	// Valida la programacion de Arduino
	char buffer[1024];

	Serial.Message    = "LABI Laser\r";
	Serial.MessageLen = 11;
	if( !Serial.Read( buffer, 256, &CSerial::FindMessage ) ) {

		// Cambia el cursor
		SetClassLong( hwnd, GCL_HCURSOR, cursor );

		// Muestra la notificacion y cierra la aplicacion
		ShowAlert( hwnd, IDS_SERIAL, IDS_SERIALID );
		PostMessage( hwnd, WM_CLOSE, 0, 0 );
		return TRUE;
	}

	// Cambia el cursor
	SetClassLong( hwnd, GCL_HCURSOR, cursor );
	return TRUE;
}


LRESULT CDlgMain::Cancel( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	EndDialog( hwnd, TRUE );
	return TRUE;
}


LRESULT CDlgMain::Validate( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Obtiene el valor de intensidad
	TCHAR string[8];
	UINT  strLen = SendDlgItemMessageA( hwnd, IDC_EDIT1, WM_GETTEXT, 8, 
						reinterpret_cast<LPARAM>( string ) );

	// Recupera el ultimo valor de intensidad valido si el campo esta vacio
	if( strLen == 0 )
		SetDlgItemInt( hwnd, IDC_EDIT1, Intensity, FALSE );

	return TRUE;
}


LRESULT CDlgMain::Send( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	SetCursor( LoadCursor( 0, IDC_WAIT ) );

	// Lectura del valor de intensidad
	char  string[8];
	UINT  strLen;

	lparam = reinterpret_cast< LPARAM >( string );
	strLen = SendDlgItemMessageA( hwnd, IDC_EDIT1, WM_GETTEXT, 8, lparam );

	// Cambia la intensidad de salida del laser
	Serial.Send( string, strLen );

	// Arduino modifica el valor si esta fuera de los limites
	strLen = Serial.Read( string, sizeof( string ), &CSerial::FindNewLine ) - 1;
	string[ strLen ] = 0;
	SendDlgItemMessageA( hwnd, IDC_EDIT1, WM_SETTEXT, 0, lparam );

	// Preserva el ultimo valor aceptado mayor que cero
	if( *string != '0' )
		Intensity = GetDlgItemInt( hwnd, IDC_EDIT1, 0, 0 );

	// Restaura el cursor
	SetCursor( LoadCursor( 0, IDC_ARROW ) ); 
	return TRUE;
}


LRESULT CDlgMain::Activate( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	SetCursor( LoadCursor( 0, IDC_WAIT ) ); 

	// Habilitar ?
	UINT index  = IsWindowEnabled( GetDlgItem( hwnd, IDC_EDIT1 ) ) == 0;
	UINT enable = 0 - index;

	// Modifica la intensidad del Laser
	SetDlgItemInt( hwnd, IDC_EDIT1, Intensity & enable, 0 );
	Send( hwnd, WM_COMMAND, 0, 0 );

	// Habilita los botones
	EnableWindow( GetDlgItem( hwnd, IDC_EDIT1 ), enable );
	EnableWindow( GetDlgItem( hwnd, IDC_SEND ), enable );

	// Cambia el valor del despliegue de intensidad
	TCHAR   *caption[2] = { _T("Encender"), _T("Apagar") };
	LPARAM  *param      = reinterpret_cast<LPARAM*>( caption );
	SendDlgItemMessage( hwnd, IDC_ONOFF, WM_SETTEXT, 0, param[ index ] );

	// Restaura el cursor
	SetCursor( LoadCursor( 0, IDC_ARROW ) ); 
	return TRUE;
}
