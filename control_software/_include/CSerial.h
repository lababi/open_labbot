#define ARDUINO_UNO       0
#define MAKER_MEX         1

// **************************************************************************************
// DEFINICION DE LA CALSE CSERIAL
// **************************************************************************************

class CSerial {

	typedef  UINT ( CSerial::*HANDLER ) ( char *, UINT );

private:

	void      QueryKey( HKEY key, UINT baudRate );
    CSerial*  OpenPort( TCHAR *portName, UINT baudRate );

public:

	static    UINT LineCounting( char * );

	 CSerial() { Port = 0; };
	~CSerial();

	 void     EnumArduinoPorts( UINT port );
	 UINT     Send( char* string, UINT strLen );
	 UINT     Read( char* storage, UINT size, CSerial::HANDLER );
	 UINT     IsConnected() { return Port != 0 ? -1 : 0; }

	 UINT     FindNewLineW( char *string, UINT strLen );
	 UINT     FindNewLine( char *string, UINT strLen );
	 UINT     FindMessage( char *string, UINT strLen );

private:

	 static   TCHAR  *KeyName[];
	 HANDLE   Port;

public:

	 char    *Message;
	 UINT     MessageLen;
	 UINT     BytesRead;
};

