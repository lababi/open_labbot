class CCoord {

	static  UINT  PrintAscii( char*, UINT );

public:

	CCoord() {};
   ~CCoord() {};

	static  UINT  Validate( TCHAR* );
	static  UINT  Print( TCHAR*, UINT );

	void  Scan( TCHAR* );
	UINT  Format2CNC( char *buffer, UINT space );

public:

	UINT  Horizontal;
	UINT  Vertical;
	UINT  Elevation;
};