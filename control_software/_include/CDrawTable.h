// **************************************************************************************
// DEFINICION DE LA CLASE CDrawTable
// **************************************************************************************

class CDrawTable {

public:

	CDrawTable() {};
   ~CDrawTable() {};

	static HFONT  SetFont( HDC );
	static void   FreeResources();

	void  DrawCell( HDC, UINT );
	void  DrawValues( HDC );
	void  DrawHeaders( HDC );
	void  DrawTitle( HDC );

	struct TString {
		TCHAR  *String;
		UINT    Length;
	};

private:

	static  HFONT  Font;
	static  HFONT  BoldFont;
	static  HBRUSH Brush;

	HGDIOBJ  LastGDI;

public:

	static  UINT   Height;
	static  UINT   ColWidth;

	UINT      nRow;
	UINT      nCol;
	UINT      Top;
	UINT     *Column;
	TString  *Label;
	TString  *Value;
};


