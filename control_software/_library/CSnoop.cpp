#include <windows.h>
#include <tchar.h>

#include "..\\_include\\CSnoop.h"

// **************************************************************************************
// AREA DE DATOS
// **************************************************************************************
TCHAR  *CSnoop::AppName;
UINT    CSnoop::Length;


// **************************************************************************************
// DETECCION DE LA VENTANA DE ADQUISICION
// **************************************************************************************
UINT CSnoop::Find( TCHAR *string, UINT length ) {

	// Busca la ventana
	AppName = string;
	Length  = length;
	Window  = 0;

	EnumWindows( 
		reinterpret_cast<WNDENUMPROC>( &ReciveWindowHandle ), 
		reinterpret_cast<LPARAM>( this ) );

	return Window != 0;
}


UINT CALLBACK CSnoop::ReciveWindowHandle( HWND hwnd, CSnoop* instance ) {

	// Obtiene el texto de la ventana
	TCHAR  caption[256];
	UINT   length   = SendMessage
		              ( hwnd, WM_GETTEXT, 256, reinterpret_cast<LPARAM>( caption ) );

	// Compara el texto
	UINT areEqual = CompareString( LOCALE_SYSTEM_DEFAULT, 0, 
						caption, length, AppName, Length ) == 2;

	// Preserva la ventana
	if( areEqual )
		instance->Window = hwnd;

	return areEqual ^ 1;
}
