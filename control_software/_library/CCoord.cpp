#include <windows.h>
#include <tchar.h>
#include "..\\_include\\CCoord.h"

#define DIV10 0x6667

// **************************************************************************************
// IMPLEMENTACION DE LA CLASE
// **************************************************************************************
UINT CCoord::PrintAscii( char *buffer, UINT next ) {

	UINT   offset = 3 + ( next > 999 ) + ( next > 9999 );
	char  *target = buffer + offset - 3;

	*target ++ = _T('0');
	*target ++ = _T('.');
	*target ++ = _T('0');

	 // El numero mayor que se puede convertir con multiplicacion es 43698
	UINT  current = 0;
	UINT   last   = next;

	do {
		next    = ( last * DIV10 ) >> 18;
		*target = last - ( next << 1 ) - ( next << 3 ) + 0x30;
		target -= 1 + ( ++current == 2 );
		last    = next;		
	} while( next );

	return offset + 1;
}


UINT CCoord::Print( TCHAR *buffer, UINT next ) {

	UINT   offset = 3 + ( next > 999 ) + ( next > 9999 );
	TCHAR *target = buffer + offset - 3;

	*target ++ = _T('0');
	*target ++ = _T('.');
	*target ++ = _T('0');

	 // El numero mayor que se puede convertir es 43698

	UINT  current = 0;
	UINT   last   = next;

	do {
		next    = ( last * DIV10 ) >> 18;
		*target = last - ( next << 1 ) - ( next << 3 ) + 0x30;
		target -= 1 + ( ++current == 2 );
		last    = next;		
	} while( next );

	return offset + 1;
}


UINT CCoord::Format2CNC( char *buffer, UINT space ) {

	static char Name[] = "XYZ"; 

	space = 2 + ( space != 0 );
	UINT   index   = 0;
	UINT  *coord   = &Horizontal;
	char  *current = buffer;

	do {
		*current ++ = Name[index ++];
		 current   += PrintAscii( current, *coord ++ );
	} while( -- space );

	*current = 0;
	return current - buffer;
}


void CCoord::Scan( TCHAR *buffer ) {

	// Eje en el primer caracter
	UINT  index = *(buffer ++) - _T('X');
	UINT  *axis = &Horizontal;

	do {
		*axis = 0;

		// Convierte la parte entera
		do {
			*axis = *axis * 10 + *buffer - 0x30;
		} while( *(++ buffer) != _T('.') );

		buffer ++;

		// Convierte los decimales
		*axis = *axis * 10 + *buffer ++ - 0x30;
		*axis = *axis * 10 + *buffer ++ - 0x30;
		 axis ++;

	} while( *buffer ++ );
}


UINT CCoord::Validate( TCHAR *buffer ) {

	UINT  integer = 0;
	UINT  decimal = 0;
	UINT  dot     = 0;
	UINT *current = &integer;

	do {

		UINT code = ( static_cast<UINT>( *buffer - 0x30 ) <= 9 ) +
					( ( *buffer == _T('.')) << 1 );

		switch( code ) {

		case 1:					// Procesa los numeros
			current[0] += 1;
			continue;

		case 2:					// Procesa los puntos
			dot ++;
			current = &decimal;
			continue;

		default:				// Error si el caracter es distinto de '.' o numero
			return FALSE;
		}

	} while( *++buffer );

	return ( static_cast<UINT>( integer - 1 ) <= 2 ) & ( decimal == 2 ) & ( dot == 1 );
}