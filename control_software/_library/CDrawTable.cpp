#include <windows.h>
#include <tchar.h>

#include "..\\_include\\CDrawTable.h"
#include "..\\_include\\Fonts.h"

#define MARGIN 4

// **************************************************************************************
// IMPLEMENTACION DE LA CLASE CDrawTable
// **************************************************************************************

HFONT   CDrawTable::Font;
HFONT   CDrawTable::BoldFont;
HBRUSH  CDrawTable::Brush;
UINT    CDrawTable::Height;
UINT    CDrawTable::ColWidth;

// **************************************************************************************
// IMPLEMENTACION DE LA CLASE CDrawTable
// **************************************************************************************

HFONT CDrawTable::SetFont( HDC hdc ) {

	// Obtiene las fuentes del sistema
	Font     = GetSystemFont( SYSFNT_DIALOG,  0 );
	BoldFont = GetSystemFont( SYSFNT_DIALOG, -1 );
	Brush    = CreateSolidBrush( GetSysColor( COLOR_MENUBAR ) );

	// Determina la altura de los renglones
	HGDIOBJ lastGDI = SelectObject( hdc, Font );

	TEXTMETRIC tm;
	GetTextMetrics( hdc, &tm );
	Height = tm.tmHeight - tm.tmInternalLeading + tm.tmDescent * 2;

	SelectObject( hdc, lastGDI );
	return Font;
}


void CDrawTable::FreeResources() {

	DeleteObject( Font );
	DeleteObject( BoldFont );
	DeleteObject( Brush );
}


void  CDrawTable::DrawTitle( HDC hdc ) {

	// Dibuja el fondo de los encabezados
	LastGDI = SelectObject ( hdc, 
				CreateSolidBrush( GetSysColor( COLOR_BTNSHADOW ) ) );
	PatBlt( hdc, Column[0], Top, Column[nCol - 1] - Column[0], Height - 2, PATCOPY );
	DeleteObject( SelectObject( hdc, LastGDI ) );

	// Dibuja el texto de los encabezados
	SetTextColor( hdc, 0xFFFFFF );
	SetBkMode( hdc, TRANSPARENT );
	SetTextAlign ( hdc, TA_TOP | TA_CENTER );
	LastGDI = SelectObject ( hdc, BoldFont );

	TextOut( hdc, 1 + Column[0] + ( Column[nCol - 1] - Column[0] ) / 2, Top, 
			 Label->String, Label->Length );

	// Reestablece HDC
	SelectObject( hdc, LastGDI );
}


void  CDrawTable::DrawHeaders( HDC hdc ) {

	// Dibuja el fondo de los encabezados
	LastGDI = SelectObject ( hdc, 
				CreateSolidBrush( GetSysColor( COLOR_BTNSHADOW ) ) );
	PatBlt( hdc, Column[0], Top, Column[nCol - 1] - Column[0], Height - 2, PATCOPY );
	DeleteObject( SelectObject( hdc, LastGDI ) );

	// Dibuja el texto de los encabezados
	SetTextColor( hdc, 0xFFFFFF );
	SetBkMode( hdc, TRANSPARENT );
	SetTextAlign ( hdc, TA_TOP | TA_LEFT );
	LastGDI = SelectObject ( hdc, BoldFont );

	TString  *label  = Label;
	UINT     *column = Column;
	UINT      row    = Top;

	TextOut( hdc, MARGIN + *column ++, row, label->String, label->Length );
	SetTextAlign ( hdc, TA_TOP | TA_RIGHT );

	do {
		label ++;
		TextOut( hdc, *column ++ - MARGIN, row, label->String, label->Length );
	} while( static_cast<UINT>( column - Column ) < nCol );

	// Reestablece HDC
	SelectObject( hdc, LastGDI );
}


void CDrawTable::DrawValues( HDC hdc ) {

	// Limpia el fondo
	HGDIOBJ oldBrush = SelectObject( hdc, Brush );
	PatBlt( hdc, *Column, Top + Height - 2, 
		    Column[nCol - 1] - *Column, nRow * Height + 2, PATCOPY );
	SelectObject( hdc, oldBrush );

	// Establece los parametros de dibujo
	SetTextAlign( hdc, TA_TOP | TA_LEFT );
	SetBkMode( hdc, TRANSPARENT );
	SetTextColor( hdc, 0 );
	SelectObject( hdc, Font );

	// Dibuja el contenido de la tabla
	TString  *label  = Value;
	UINT     *column = Column;
	UINT      i, row, margin = MARGIN;

	do {

		i    = nRow;
		row  = Top;

		do {
			row += Height;
			TextOut( hdc, *column + margin, row, label->String, label->Length );
			label ++;
		} while( -- i );

		SetTextAlign( hdc, TA_TOP | TA_RIGHT );
		margin = -MARGIN;

	} while( static_cast<UINT>( ++ column - Column ) < nCol );

	SelectObject( hdc, LastGDI );
}


void CDrawTable::DrawCell( HDC hdc, UINT index ) {


	// Determina las coordenadas para el dibujo
	index      += nRow;
	UINT column = index / nRow;
	UINT row    = index % nRow;
	RECT rc     = { 
		Column[ column ] - ColWidth, Top + Height * ( row + 1 ) - 1,
		Column[ column ], Top + Height * ( row + 2 ) - 1
	};

	LastGDI = SelectObject( hdc, Font );
	SetTextAlign( hdc, TA_TOP | TA_RIGHT );
	SetBkColor( hdc, GetSysColor( COLOR_MENU ) );

	ExtTextOut( hdc, Column[ column ] - MARGIN, Top + Height * ( row + 1 ),
				ETO_OPAQUE, &rc, Value[ index ].String, Value[ index ].Length, 0 );

	SelectObject( hdc, LastGDI );
}