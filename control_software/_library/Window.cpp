#include <windows.h>

#define IDS_STRING1  1
#define IDS_STRING2  IDS_STRING1 + 1
#define IDI_ICON1    100
#define CLASS_NAME   32
#define CAPTION      128


//***************************************************************************************
// DEFINICIONES PARA EL STACK DE LOS PROCEDIMIENTOS DE VENTANA
//***************************************************************************************

struct SWndStack {

	DWORD   This;

	DWORD   Address;
	HWND    hwnd;
	UINT    msg; 
	WPARAM  wparam; 
	LPARAM  lparam;
};


struct ClassWndStack {

	DWORD   This;
	DWORD   BaseReg;

	DWORD   Address;
	HWND    hwnd;
	UINT    msg; 
	WPARAM  wparam; 
	LPARAM  lparam;
};


struct SNWndStack {

	DWORD   SIndex;
	DWORD   DIndex;
	DWORD   BaseReg;
	DWORD   Table;
	DWORD   This;

	DWORD   Address;
	HWND    hwnd;
	UINT    msg; 
	WPARAM  wparam; 
	LPARAM  lparam;
};



//***************************************************************************************
// REGISTRO Y CREACION DE LA VENTANA PRINCIPAL
//***************************************************************************************

HWND CreateFrame
( HINSTANCE instance, WNDPROC wndProc, UINT style, HMENU menu ) {

	// Obtiene el nombre de la clase

	TCHAR className[CLASS_NAME];
	LoadString(instance, IDS_STRING1, className, CLASS_NAME);

	// Registra la clase de ventana

	WNDCLASSEX  wndclass;

	wndclass.cbSize        = sizeof(WNDCLASSEX);
	wndclass.style         = 0;
	wndclass.lpfnWndProc   = wndProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = 0;
	wndclass.hInstance     = instance;
	wndclass.hIcon         = LoadIcon(instance, MAKEINTRESOURCE(IDI_ICON1));
	wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = 0;
	wndclass.lpszMenuName  = 0;
	wndclass.lpszClassName = className;
	wndclass.hIconSm       = (HICON) LoadImage(instance, MAKEINTRESOURCE(IDI_ICON1), 
								IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);

	if(!RegisterClassEx(&wndclass)) return 0;

	// Obtiene el nombre de la ventana

	TCHAR caption[CAPTION];
	LoadString(instance, IDS_STRING2, caption, CAPTION);

	// Crea la ventana

	return CreateWindowEx(
				0,
				className, 
				caption, 
				WS_OVERLAPPEDWINDOW + style, 
				CW_USEDEFAULT, CW_USEDEFAULT, 
				CW_USEDEFAULT, CW_USEDEFAULT, 
				NULL, 
				menu, 
				instance, 
				NULL);
}



//***************************************************************************************
// REGISTRO DE UN CONTROL
//***************************************************************************************

UINT RegisterControl
( HINSTANCE instance, TCHAR *className, WNDPROC wndProc, UINT style ) {

	// Registra la clase de ventana

	WNDCLASSEX  wndclass;

	wndclass.cbSize        = sizeof(WNDCLASSEX);
	wndclass.style         = style;
	wndclass.lpfnWndProc   = wndProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = 0;
	wndclass.hInstance     = instance;
	wndclass.hIcon         = 0;
	wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = 0;
	wndclass.lpszMenuName  = 0;
	wndclass.lpszClassName = className;
	wndclass.hIconSm       = 0;

	return RegisterClassEx( &wndclass );
}



//***************************************************************************************
// RUTINAS PARA MODIFICAR EL ASPECTO DE LAS VENTANAS
//***************************************************************************************

void CenterWindow( HWND hwnd ) {

	// Centra el cuadro en la pantalla

	RECT rc;
	GetWindowRect(hwnd, &rc);

	rc.right  -= rc.left;
	rc.bottom -= rc.top;
	rc.left    = (GetSystemMetrics(SM_CXSCREEN) - rc.right) / 2;
	rc.top     = (GetSystemMetrics(SM_CYSCREEN) - rc.bottom) / 2;

	MoveWindow(hwnd, rc.left, rc.top, rc.right, rc.bottom,  FALSE);
}

	

void SetIconWindow( HINSTANCE instance, HWND hwnd, int resourceID ) {

	union {
		HICON  icon;
		LPARAM lparam;
	};

	// Carga el icono de la aplicacion

	icon = LoadIcon( instance, MAKEINTRESOURCE(resourceID ));
	SendMessage( hwnd, WM_SETICON, ICON_BIG, lparam );
}


void SetControlSize( HWND hwnd, UINT width, UINT height ) {

	RECT rc;
	GetWindowRect( hwnd, &rc );
	ScreenToClient( GetParent( hwnd ), reinterpret_cast<LPPOINT>( &rc ) );
	SetWindowPos( hwnd, 0, rc.left, rc.top, width, height,
				  SWP_NOMOVE | SWP_NOZORDER );

}


void GetControlRect( HWND hwnd, RECT *rc ) {

	HWND parent = GetParent( hwnd );

	GetWindowRect( hwnd, rc );
	ScreenToClient( parent, reinterpret_cast<LPPOINT>( rc ) );
	ScreenToClient( parent, reinterpret_cast<LPPOINT>( &rc->right ) );
}


void SetWindowRect( HWND hwnd, UINT width, UINT height ) {

	RECT rc;
	rc.left   = 0;
	rc.top    = 0;
	rc.right  = width;
	rc.bottom = height;

	// Las dimensiones varian con el estilo de cada ventana
	AdjustWindowRectEx( &rc, GetWindowLong( hwnd, GWL_STYLE ), 
						0, GetWindowLong( hwnd, GWL_EXSTYLE ) );

	SetWindowPos( hwnd, 0, 0, 0, rc.right - rc.left,
				  rc.bottom - rc.top, SWP_NOMOVE | SWP_NOZORDER );
}


//***************************************************************************************
// RUTINAS PARA OBTENER FUENTES DEL SISTEMA
//***************************************************************************************

HFONT GetIconFont() {

	// Obtiene la descripcion de la fuente utilizada en los iconos del sistema

	LOGFONT logFont;
	SystemParametersInfo( SPI_GETICONTITLELOGFONT, sizeof(LOGFONT), &logFont, 0 );

	// Crea la fuente

	return CreateFontIndirect( &logFont );
}



HFONT GetSystemFont( UINT type, UINT bold ) {

	// Obtiene la informacion del sistema

	NONCLIENTMETRICS  metrics;

	metrics.cbSize = sizeof( NONCLIENTMETRICS );
	SystemParametersInfo( SPI_GETNONCLIENTMETRICS, 0, &metrics, 0 );

	// Selecciona la informacion de la fuente

	LOGFONT  *logFont[6] = {
		&metrics.lfCaptionFont, &metrics.lfSmCaptionFont, &metrics.lfMenuFont, 
		&metrics.lfStatusFont,  &metrics.lfMessageFont
	};

	// Coloca la fuente en negritas

	logFont[type]->lfWeight += ( bold ? -1 : 0 ) & 300;

	return CreateFontIndirect( logFont[type] );
}


//***************************************************************************************
// MESSAGEBOX
//***************************************************************************************
UINT ShowAlert( HWND hwnd, UINT idTitle, UINT msg ) {
	
	HINSTANCE instance = (HINSTANCE) GetWindowLong( hwnd, GWL_HINSTANCE );

	// Carga la leyenda de la ventana
	TCHAR caption[256];
	LoadString( instance, idTitle, caption, 256 );

	// Carga el mensaje de error
	TCHAR legend[256];
	LoadString( instance, msg, legend, 256 );

	// Despliega la caja de dialogo
	return MessageBox( hwnd, legend, caption, MB_OK | MB_ICONERROR );
}


//***************************************************************************************
// PROCEDIMIENTOS DE VENTANA
//***************************************************************************************

__declspec(naked) LRESULT __stdcall SWndProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	__asm{

		push  ecx

		mov   eax, [esp]SWndStack.msg
		mov   ecx, [edx + 4]
		mov   edx, [edx]

next:	cmp   eax, [edx]
		je    exec

		add   edx, 8
		sub   ecx, 1
		jnz   next

		add   esp, 4
		jmp   dword ptr DefWindowProc

exec:	mov   edx, [edx + 4]
		mov   ecx, [esp]SWndStack.This              // Restaura _this
		add   esp, 4

		jmp   edx
	}
}



__declspec(naked) LRESULT __stdcall ClassWndProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	__asm{

		push  ebx
		push  ecx

		mov   ebx, [edx]
		mov   ecx, [edx + 4]                           // &EventInfoClass.Elements
		mov   eax, [esp]ClassWndStack.msg

		lea   ebx, [ebx + ecx*8 - 8]
		mov   edx, [edx + 8]                           // &EventInfoClass.Offset

next:	cmp   eax, [ebx]
		je    exec

		sub   ebx, 8
		sub   ecx, 1
		jnz   next

		mov   ebx, [esp]ClassWndStack.BaseReg
		add   esp, 8
		jmp   dword ptr DefWindowProc

exec:	mov   eax, [edx + ecx*4 - 4]                   // Class Offset
		mov   edx, [ebx + 4]                           // &Event[ecx].Dispatch
		mov   ecx, [esp]ClassWndStack.This             // Restaura _this
		
		mov   ebx, [esp]ClassWndStack.BaseReg
		add   ecx, eax
		add   esp, 8

		jmp   edx
	}
}


//***************************************************************************************
// PROCEDIMIENTOS PARA CUADROS DE DIALOGO
//***************************************************************************************

__declspec(naked) LRESULT __stdcall SDlgProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	__asm{

		push  ecx
		mov   ecx, [esp]SWndStack.wparam
		mov   eax, [esp]SWndStack.msg

		xor   ecx, eax
		sub   eax, WM_COMMAND

		push  ebx
		mov   ebx, 0
		cmp   eax, 1

		sbb   ebx, 0                                   // msg == WM_COMMAND ? -1 : 0
		add   eax, WM_COMMAND

		and   ecx, ebx
		and   ebx, 1

		xor   eax, ecx
		lea   edx, [edx + ebx*8]
		pop   ebx
		
		mov   ecx, [edx + 4]
		mov   edx, [edx]

next:	cmp   eax, [edx]
		je    exec

		add   edx, 8
		sub   ecx, 1
		jnz   next

		add   esp, 4
		xor   eax, eax
		ret   16

exec:	mov   edx, [edx + 4]
		mov   ecx, [esp]SWndStack.This                 // Restaura _this
		add   esp, 4

		jmp   edx
	}
}


__declspec(naked) LRESULT __stdcall SNDlgProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	__asm{

		// Preserva los registros
	
		push  ecx
		push  edx

		push  ebx
		push  edi
		push  esi

		mov   ebx, [esp]SNWndStack.msg
		mov   ecx, [esp]SNWndStack.lparam
		lea   esi, [esp]SNWndStack.wparam              // & wparam

		lea   eax, [ebx - WM_COMMAND]
		add   ecx, 8								   // & lparam.code
		mov   edx, 0

		xor   ecx, esi
		cmp   eax, 1
		lea   eax, [ebx - WM_NOTIFY]

		adc   edx, -1                                  // msg == COMMAND ? 0 : -1
		mov   edi, 0
		cmp   eax, 1

		and   ecx, edx
		cmp   eax, 1
		lea   ebx, [esp]SNWndStack.msg
		
		adc   edi, -1                                  // msg == NOTIFY ? 0 : -1
		xor   ecx, esi
		lea   eax, [edx + 1]

		and   edx, edi
		xor   ebx, ecx
		lea   eax, [edi + eax*2 + 1]

		and   edx, ebx
		mov   esi, [esp]SNWndStack.Table
		mov   edi, [esp]SNWndStack.DIndex	           // Restaura EDI

		xor   edx, ecx
		lea   esi, [esi + eax*8]
		mov   ebx, [esp]SNWndStack.BaseReg           // Restaura EBX
		
		mov   eax, [edx]
		mov   edx, [esi]
		mov   ecx, [esi + 4]

		mov   esi, [esp]SNWndStack.SIndex            // Restaura ESI	
next:	cmp   eax, [edx]
		je    exec

		add   edx, 8
		sub   ecx, 1
		jnz   next

		add   esp, 20
		xor   eax, eax
		ret   16

exec:	mov   edx, [edx + 4]
		mov   ecx, [esp]SNWndStack.This              // Restaura _this
		add   esp, 20

		jmp   edx
	}
}



//***************************************************************************************
// PROCEDIMIENTOS PARA CONTROLES SUPERCLASIFICADOS
//***************************************************************************************

struct SupWndStack {

	DWORD   This;
	DWORD   BaseReg;
	DWORD   BaseProc;
	DWORD   Address;
	HWND    hwnd;
	UINT    msg; 
	WPARAM  wparam; 
	LPARAM  lparam;
};


__declspec(naked) LRESULT __stdcall SupWndProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	__asm{

		push  eax                                      // Preserva direccion de BaseProc
		push  ebx                                      
		push  ecx                                      // Presrva this

		mov   ebx, [edx]
		mov   ecx, [edx + 4]                           // &EventInfoClass.Elements
		mov   eax, [esp]SupWndStack.msg

		lea   ebx, [ebx + ecx*8 - 8]
		mov   edx, [edx + 8]                           // &EventInfoClass.Offset

next:	cmp   eax, [ebx]
		je    exec

		sub   ebx, 8
		sub   ecx, 1
		jnz   next

		mov   ebx, [esp]SupWndStack.BaseReg			   // Restaura ebx
		mov   ecx, [esp]SupWndStack.BaseProc           // Rutina en ecx
		mov   eax, [esp]SupWndStack.Address            // Direccion de regreso en eax

		mov   [esp]SupWndStack.Address, ecx
		mov   [esp]SupWndStack.BaseProc, eax
		add   esp, 8                                   // Ajusta el stack

		jmp   dword ptr CallWindowProc

exec:	mov   edx, [ebx + 4]                           // &Event[ecx].Dispatch
		mov   ecx, [esp]SupWndStack.This               // Restaura _this
		mov   ebx, [esp]SupWndStack.BaseReg			   // Restaura ebx

		add   esp, 12
		jmp   edx
	}
}


//***************************************************************************************
// PROCEDIMIENTOS PARA CONTROLES SUBCLASIFICADOS
//***************************************************************************************

struct SubWndStack {

	DWORD   This;
	DWORD   BaseReg;
	DWORD   Adress;
	DWORD   CtrlProc;
	HWND    hwnd;
	UINT    msg; 
	WPARAM  wparam; 
	LPARAM  lparam;
};


struct SubExecStack {

	DWORD   xProc;
	DWORD   xhwnd;
	DWORD   xmsg;
	DWORD   This;
	DWORD   BaseReg;
	DWORD   Adress;
	DWORD   CtrlProc;
	HWND    hwnd;
	UINT    msg; 
	WPARAM  wparam; 
	LPARAM  lparam;
};


__declspec(naked) LRESULT __stdcall SubWndProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	__asm {

		push eax
		push ebx
		push ecx

		mov  ecx, [edx + 4]								// Numero de eventos 
		mov  edx, [edx]									// &EventInfo
		mov  ebx, [esi]SubWndStack.CtrlProc

		mov  [esi]SubWndStack.CtrlProc, eax				// Arregla el stack para CallWindowProc
		mov  [esi]SubWndStack.Adress, ebx				
		mov  ebx, [esp]SubWndStack.BaseReg
		mov  eax, [esi]SubWndStack.msg

next:	cmp   eax, [edx]
		je    exec

		add   edx, 8
		sub   ecx, 1
		jnz   next

		add   esp, 8
		jmp   dword ptr CallWindowProc

exec:	mov   ecx, [esp]SWndStack.hwnd
		sub   esp, 12
		mov   edx, [edx + 4]							// Dispatch

		mov   [esp]SubExecStack.xhwnd, ecx
		mov   [esp]SubExecStack.xmsg, eax
		mov   eax, [esp]SubExecStack.wparam

		mov   ecx, [esp]SubExecStack.This
		mov   [esp]SubExecStack.This, eax
		mov   eax, [esp]SubExecStack.lparam

		mov   [esp]SubExecStack.xProc, offset CallWindowProc
		mov   [esp]SubExecStack.BaseReg, eax
		jmp   edx
	}
}